<?php declare(strict_types=1);

namespace JonathanMartz\RedisCatalog\Controller;

use Shopware\Core\Content\Category\CategoryEntity;
use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Shopware\Core\Content\Product\ProductCollection;
use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;


/**
 * @Route(defaults={"_routeScope"={"store-api"}})
 */
class CategoriesController extends StorefrontController
{
    /**
     * @Route("/store-api/redis-catalog/categories", name="store-api.categories", methods={"GET"})
     */
    public function view(Context $context)
    {
        $redis = RedisAdapter::createConnection(
            'redis://redis'
        );

        if (!empty($redis->get('categories')) && strlen($redis->get('categories')) > 10) {
            return new JsonResponse(json_decode($redis->get('categories')));
        }

        $category = $this->getCategories($context);
        $redis->set('categories', json_encode($category));
        return new JsonResponse($category);
    }


    public function getCategories($context): EntitySearchResult
    {
        $categoryRepository = $this->container->get('category.repository');
        $criteria = new Criteria();
        $criteria->addAssociation('children');
        $category = $categoryRepository->search($criteria, $context);
        return $category;
    }
}