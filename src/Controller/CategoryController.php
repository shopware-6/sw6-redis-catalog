<?php declare(strict_types=1);

namespace JonathanMartz\RedisCatalog\Controller;

use JonathanMartz\RedisCatalog\Service\ReadingData;
use Shopware\Core\Content\Category\CategoryEntity;
use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Shopware\Core\Content\Product\ProductCollection;
use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;

/**
 * @Route(defaults={"_routeScope"={"store-api"}})
 */
class CategoryController extends StorefrontController
{
    /**
     * @Route("/store-api/redis-catalog/category/{id}", name="store-api.category.view", methods={"GET"})
     */
    public function view(Context $context, string $id)
    {
        $redis = RedisAdapter::createConnection(
            'redis://redis'
        );

        if (!empty($redis->get('category-' . $id)) && strlen($redis->get('category-' . $id)) > 10) {
            return new JsonResponse(json_decode($redis->get('category-' . $id)));
        }

        $category = $this->getCategoryById($id, $context);
        $redis->set('category-'.$id, json_encode($category));
        return new JsonResponse($category);
    }


    public function getCategoryById($id, $context): CategoryEntity
    {
        $categoryRepository = $this->container->get('category.repository');
        $criteria = new Criteria([$id]);
        $criteria->addAssociation('children');
        $criteria->addAssociation('products');
        $category = $categoryRepository->search($criteria, $context)->first();
        return $category;
    }
}