<?php declare(strict_types=1);

namespace JonathanMartz\RedisCatalog\Controller;


use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Content\Product\ProductEntity;

/**
 * @Route(defaults={"_routeScope"={"store-api"}})
 */
class ProductController extends StorefrontController
{

    /**
     * @Route("/store-api/redis-catalog/product/{id}", name="store-api.product.view", methods={"GET"})
     */
    public function view(Context $context, string $id)
    {

        $redis = RedisAdapter::createConnection(
            'redis://redis'
        );

        if (!empty($redis->get('product-' . $id)) && strlen($redis->get('product-' . $id)) > 10) {
            return new JsonResponse(json_decode($redis->get('product-' . $id)));
        }

        $product = $this->getProductById($id, $context);
        $redis->set('product-' . $id, json_encode($product));
        return new JsonResponse($product);
    }


    public function getProductById($id, $context): ProductEntity
    {
        $productRepository = $this->container->get('product.repository');
        $criteria = new Criteria([$id]);
        $criteria->addAssociation('options');
        $criteria->addAssociation('cover');
        $product = $productRepository->search($criteria, $context)->first();
        return $product;
    }
}